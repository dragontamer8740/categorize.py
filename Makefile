CC=cc
CFLAGS=-Wall `pkg-config --cflags gtk+-3.0 glib-2.0`
LDFLAGS=`pkg-config --libs gtk+-3.0 glib-2.0`

.PHONY: all

all: categorize

categorize: categorize.c
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS)

.PHONY: clean

clean:
	rm -f categorize

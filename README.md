Image/file categorizer for mcomix. Good luck figuring out how it's supposed to
work (hint: (`program_name (full path to file to operate on)`).

I basically only wrote it for my personal use.

Originally, this was a Python program. That's still here.

But the version you might want to use instead is under `fltk`.

It's a 'hybrid' program, since it uses a shell script wrapper so i don't have
to bend my brain on memory management errors quite as much.

The other fltk version (`purec`) has memory errors that I am too lazy to solve
right now. Don't use it.

The main benefit of `fltk_hybrid` is that it runs much, much faster than the
original Python program does, both in startup and total run time. It also
affords you two more options of file chooser UI's, in case you dislike the
GTK3 picker: FLTK's own file chooser and whatever `Fl_native_file_chooser()`
decides to use. It prioritizes the GTK2 chooser over the GTK3 one, if
available, and GTK3's chooser over the FLTK Chooser. On non-X/Unix platforms
it uses the OS's file chooser API, where applicable (Mac OS X and Windows).

You can edit the scripts (`categorize_fltk` and `categorize_fltk_mv`) to make
changes to most preferences. Note that the binaries' location needs to be in
your PATH variable for the scripts to work. You may have to edit the .cpp file
to switch fonts for now. That might be changed in the future to use environment
variables in the script wrapper instead. It'd be an easy change.

Another upshot of the FLTK version is that the window resizes nicely and
starts even more quickly than a GTK3 C version would.

#### Optional FLTK patch

For the FLTK version, when using the FLTK file chooser, I also made one patch
to the toolkit sources themselves. It's totally optional, but if you do it it
will allow you to create new categories from the browser.

And since the Makefile is set up for static linking, you don't even have to
have that patched version installed system-wide.

1. Get the FLTK sources; I recommend 1.4.x (currently pre-release) since you
   can do --enable-pango on it and get text that will fall back to other fonts
   if the primary one doesn't have the right glyphs. You can also use 1.3.
   
   * Do not use 1.3.5!!!!! That's the version in the Debian Sid repositories
     right now, but it has bugs preventing input method editors (IME's) from
     working, so I can't type CJK text in it for instance. As of this writing,
     1.3.8 is the current stable release, which I recommend if you don't want
     to use 1.4.x just yet.
     
2. Edit `src/Fl_File_Chooser.cxx`. Find the function with the signature
   `void Fl_File_Chooser::type(int t)`.
   
3. Replace:
   
```
if (t & DIRECTORY)
  fileList->filetype(Fl_File_Browser::DIRECTORIES);
```
   
with:
   
```
if (t & DIRECTORY) {
  fileList->filetype(Fl_File_Browser::DIRECTORIES);
  newButton->activate();
}
```

...and then `configure`, `make`, and (optionally) `make install`. If you don't
do the `make install` you may have to do a little bit of work to make it link
to your patched library. If you know much about C/C++ linking, you will
probably be able to figure it out.

----

Original README:

Image/file categorizer for mcomix. Good luck figuring out how it's supposed to
work. I basically only wrote it for me, so it's pretty sketchy and bad. But it
works for me for now.

Uses PyGObject, Python 3, and more stuff probably at this point.
It fulfills it's purpose, though, and I'm sick of GTK for a while.

It's mainly here so I don't lose it and have to write it again.

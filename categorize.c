/* categorize.c: categorizer tool in GTK3 for Unix (primarily) */

/*
  egads, the memory leaks of a not-very-good C programmer dealing with
  GTK for the first time and not reading the documentation well enough
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>

/* POSIX: for symlink(3) */
#include <unistd.h>
/* POSIX: basename(3) */
#include <libgen.h>

/* gtk3: */
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <glib.h>
#include <glib/gstdio.h>

int dir_exists(gchar *path);
int line_length(FILE *file);
char *persist_file_read(char *path);
int persist_file_write(char *path, char *string);
int do_link(const char *path1, const char *path2);

int arg_ct=0;

GtkWidget *window;
GtkLabel *targetLabelName;

static void activate(GtkApplication* applet, gpointer user_data);
void choose_dlg(GtkWidget *widget, gpointer data);
void ok_dlg(GtkWidget *widget, gpointer data);
void close_dlg(GtkWidget *widget, gpointer data);

/* I hate callbacks */
GtkFileChooserDialog *dialog;
GtkFileChooser *chooser;

gchar *root_path;
char *selected_path;
gchar *config_path;
gchar *in_file;

const char *subdir_root="/root";
const char *persist_file="/categorize"; /* $XDG_CONFIG_DIR/categorize */
const char *app_name="Categorize"; /* for FVWM to be happy */


int main(int argc, char **argv)
{
  /* persistent storage file */
  /* try to make if nonexistent, otherwise try to read it */
  arg_ct=argc;
  FILE *conf_file;
  const gchar *xdg_config_dir=g_get_user_config_dir();
  config_path=malloc(strlen(xdg_config_dir) + strlen(persist_file) + 1);
  strcpy(config_path, xdg_config_dir);
  strncat(config_path, persist_file, strlen(persist_file));
  selected_path=persist_file_read((char *)config_path);
  if(!selected_path) /* if null pointer */
  {
    printf("Attempting to create file %s\n",config_path);
    conf_file=fopen(config_path,"wb");
    fclose(conf_file);
    selected_path=persist_file_read((char *)config_path);
  }
  if(!selected_path)
  { /* we've tried to create the file now, but are still failing. Exit. */
    fprintf(stderr,"Creation of config file %s failed. Exiting.\n",config_path);
    g_free(config_path);
    exit(2);
  }
  /* selected_path now contains a string that was malloc'd by
     persist_file_read()*/

  
  /* check that our home dir exists/is valid */
  const gchar *home_dir=g_get_home_dir();
  if(!dir_exists((gchar *)home_dir))
    {
      fprintf(stderr,"Error: Could not find a valid home directory.\nMake sure %s is a directory.\n",(char *)home_dir);
      g_free(config_path);
      g_free(selected_path);
      exit(3);
    }

  /* append subdir path to the home dir path */
  root_path=malloc(strlen(home_dir) + strlen(subdir_root) + 1);
  strcpy(root_path, home_dir);
  strncat(root_path, subdir_root, strlen(subdir_root));
  /*  printf("%s\n",root_path);*/

  if(!dir_exists((gchar *)root_path))
  {
    printf("Making new directory %s.\n",(char *)root_path);
    int err=g_mkdir ((const gchar *)root_path,0755);
    if(!dir_exists((gchar *)root_path) || err)
    {
      fprintf(stderr,"Error: tried to create directory %s, but something failed!\nThere may be a file of the same name, or a permissions problem.\n",(char *)root_path);
      fprintf(stderr,"If the directory does now exist, it's possible I coded something wrong\nwith this if guard. My bad in that case.\n");
      g_free(config_path);
      g_free(selected_path);
      g_free(root_path);
      exit(4);
    }
  }
  if(strlen(selected_path)==0)
  {
    /* empty string in session file */
    printf("Previous session likely corrupt; erasing contents and using defaults.\n");
    g_free(selected_path);
    selected_path=malloc((strlen(root_path) + 1) * sizeof(char));
    strcpy(selected_path,(char *)root_path);
  }

  /* cli args: file to link to */
  if(arg_ct > 1)
  {
    /* I think I may not need to do this for CLI args... but unsure. */
/*    in_file=malloc((strlen((gchar *)argv[1]) + 1) * sizeof(char));
      strcpy((char *)in_file, argv[1]);*/
    in_file=argv[1];
  }
  else
  {
    printf("Error: No input file name given. I need something to link to!\n");
    printf("(Please give a full path to the file, too.)\n");
    g_free(config_path);
    g_free(selected_path);
    g_free(root_path);
    exit(1);
  }
  /* sanity checks done, time to start doing stuff I guess.*/

/* ///////////////////////////////////////////////////////////////////// */
/*                            MAIN UI STUFF                              */
/* ///////////////////////////////////////////////////////////////////// */
  GtkApplication *applet;
  int status;
  applet = gtk_application_new("org.dragontamer8740.categorize", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (applet, "activate", G_CALLBACK(activate), NULL);
  /* use this instead of `gtk_window_set_wmclass()`, which is deprecated: */
  g_set_prgname(app_name);
  /* 1 instead of argc so I can handle arguments myself like a big boy */
  status = g_application_run (G_APPLICATION (applet), 1, argv);
  g_object_unref (applet);
/* ///////////////////////////////////////////////////////////////////// */
/*                          MAIN UI STUFF END                            */
/* ///////////////////////////////////////////////////////////////////// */  
  /* End: we malloc'd stuff earlier, free that stuff now */
  g_free(config_path);
  g_free(selected_path);
  g_free(root_path);
  return status;
}

/* return false (0) if failed, true (!0) otherwise */
int dir_exists(gchar *path)
{
  /* g_get_home_dir does not guarantee NULL if home dir isn't found. */
  GDir *dir_temp=g_dir_open(path, 0, NULL); /* NULL could be a GError */
  if(dir_temp==NULL)
  {
    return 0;
  }
  else
  {
    /* g_dir_open() performs a malloc. Since we were only testing
    existence, we have to free() if it existed. */
    g_free(dir_temp);
    return 1;
  }
}


int line_length(FILE *file)
{
  char a='\0';
  int i=0;
  a=getc(file); /* length 0 means empty file */
  while(a != '\n' && a != EOF)
  {
    a=getc(file);
    i++;
  }
  return i;
}


char *persist_file_read(char *path)
{
  FILE *persist_file=fopen(path, "rb"); /* 'b' is for windows */
  char *string='\0';
  if(persist_file==NULL)
  {
    return '\0';
  }
  else
  {
    /* get first line length sans newline */
    int pathlen=line_length(persist_file);
    /* seek to beginning of file again */
    fseek(persist_file, 0, SEEK_SET);
    string=malloc((pathlen + 1) * sizeof(char));
    string[0]='\0'; /* maybe unnecessary */
    /* read that line into memory, sans newline character */
    fgets(string, pathlen + 1, persist_file);
    printf("%s Opened successfully\n",path);
    fclose(persist_file);
  }
  return string;
}

int persist_file_write(char *path, char *string)
{
  FILE *persist_file=fopen(path, "wb");
  if(persist_file==NULL)
  {
    /* error */
    return -1;
  }
  fprintf(persist_file,"%s\n",string);
  fclose(persist_file);
  return 0;
}

int do_link(const char *path1, const char *path2){
  /* path1: full path to file */
  /* path2: directory to add symlink into */
  /* (this function will add the filename to the dir name) */
  int status=0;
  char *target_path;
  
  
  /* g_path_get_basename allocates!! So does g_file_parse_name!!*/
/*  GFile *base_deriving_path=g_file_parse_name(path1);
    char *link_basename=(char *)g_path_get_basename(base_deriving_path);*/
  char *link_basename=basename((char*)path1);

  
  


  /* +1 for '/', +1 for null terminator */
  printf("malloc: %lu\n",(strlen(link_basename) + strlen(selected_path) + 2) * sizeof(char));
  char *concatenation=malloc((strlen(link_basename) + strlen(selected_path) + 2) * sizeof(char));
  strcpy(concatenation,selected_path);
  strcat(concatenation,"/");
  strcat(concatenation,link_basename);
  /* also allocates! */
  GFile *link_deriving_path=g_file_parse_name(concatenation);
  char *link_fullpath=(char *)g_file_get_path(link_deriving_path);

  printf("symlink: %s -> %s\n",path1,link_fullpath);
  status=symlink(path1,link_fullpath);
/* status=symlink();*/
/*  g_free(base_deriving_path);*/


  g_free(concatenation);

/*  g_free(link_basename);*/
  g_free(link_fullpath);
/*  g_free(link_deriving_path);*/
  return status;
}

/* gtk callback */
void choose_dlg(GtkWidget *widget, gpointer data)
{
  gtk_file_chooser_set_current_folder((GtkFileChooser *)dialog, root_path);
  GFile *default_path=g_file_parse_name(root_path);
  gtk_file_chooser_set_current_folder_file((GtkFileChooser *)dialog, default_path, NULL);
  /* selected_path is global */
  gint dlg_response;
  dlg_response = gtk_dialog_run(GTK_DIALOG(dialog));
  if(dlg_response == GTK_RESPONSE_ACCEPT)
  {
    char *filename;
    chooser = GTK_FILE_CHOOSER (dialog);
    printf("Select clicked\n");
    printf("Dir selected: %s\n",gtk_file_chooser_get_filename(chooser));
    gtk_label_set_text(targetLabelName,gtk_file_chooser_get_filename(chooser));
    if(selected_path)
    {
      free(selected_path);
    }
    selected_path=malloc(strlen(((char *)gtk_file_chooser_get_filename(chooser)) + 1) * sizeof(char));
    strcpy(selected_path,(char *)gtk_file_chooser_get_filename(chooser));
  }
  else if(dlg_response == GTK_RESPONSE_CANCEL)
  {
    printf("Cancel clicked\n");
  }
  gtk_widget_hide((GtkWidget *)dialog);
}

void ok_dlg(GtkWidget *widget, gpointer data){
  // global config_path (persist_file)
  // global selected_path
  // global in_file
  // global dialog
  // global chooser
  char *cat=selected_path;
  if(strlen(cat) > 0)
  {
    printf("Attempting to save name %s to file.\n",cat);
    persist_file_write(config_path, cat);
  }
  else
  {
    printf("Path is somehow null; not saving it to persist file.\n");
  }
  int a=do_link((const char*)in_file,(const char*)cat);
  if(a) /* not equal to zero */
  {
    fprintf(stderr, "Error making symbolic link: %s\n", strerror(errno));
  }
  close_dlg(widget,data);

  /* double free was here. I think. */
/*free(config_path);
  free(selected_path);
  free(root_path);
  if(arg_ct > 1)
  {
    free(in_file);
    }*/
}

void close_dlg(GtkWidget *widget, gpointer data){
  gtk_widget_hide(window);
  gtk_widget_destroy(window);
  gtk_main_quit();
}

static void activate(GtkApplication* applet, gpointer user_data)
{
  /* pretty much transcribed from python version */
  window = gtk_application_window_new (applet);
  gtk_window_set_title (GTK_WINDOW (window), "Categorize");

  GtkLabel *inLabel=(GtkLabel *)gtk_label_new("File: ");
  gtk_label_set_justify(inLabel, GTK_JUSTIFY_RIGHT);
  gtk_widget_set_halign((GtkWidget *)inLabel, GTK_ALIGN_END);

  GtkLabel *inLabelName=(GtkLabel *)gtk_label_new(in_file); /* global */
  gtk_label_set_justify(inLabelName, GTK_JUSTIFY_LEFT);
  gtk_widget_set_halign((GtkWidget *)inLabelName, GTK_ALIGN_END);

  GtkLabel *targetLabel=(GtkLabel *)gtk_label_new("Category: ");
  gtk_label_set_justify(targetLabel, GTK_JUSTIFY_RIGHT);
  gtk_widget_set_halign((GtkWidget *)targetLabel, GTK_ALIGN_END);

  targetLabelName=(GtkLabel *)gtk_label_new(selected_path);
  gtk_label_set_justify(targetLabelName, GTK_JUSTIFY_LEFT);
  gtk_widget_set_halign((GtkWidget *)targetLabelName, GTK_ALIGN_END);
/* dialog is global because callbacks are dumb */
  dialog =
    (GtkFileChooserDialog *)
    gtk_file_chooser_dialog_new("Select a folder",
                                NULL,
                                GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
                                NULL,NULL);
  gtk_dialog_add_buttons((GtkDialog *)dialog,
                         "_Cancel",
                         GTK_RESPONSE_CANCEL,
                         "_Select",
                         GTK_RESPONSE_ACCEPT,
                         NULL);
  gtk_window_set_default_size((GtkWindow *)dialog, 400, 200);
  /* "in general, you should not use this function" */
  /* HA */
  gtk_file_chooser_set_current_folder((GtkFileChooser *)dialog, root_path);
  GFile *default_path=g_file_parse_name(root_path);
  gtk_file_chooser_set_current_folder_file((GtkFileChooser *)dialog, default_path, NULL);
  gtk_label_set_label(targetLabelName, selected_path);

  GtkBox *vbox=(GtkBox *)gtk_box_new(GTK_ORIENTATION_VERTICAL, 6);
  
  GtkButton *choose_button=(GtkButton *)gtk_button_new_with_label("Choose category");
  gtk_widget_set_tooltip_text((GtkWidget*)choose_button, "Select a category folder");

  
  g_signal_connect(choose_button, "clicked", G_CALLBACK(choose_dlg), NULL);
  
  GtkButton *ok_button=(GtkButton *)gtk_button_new_with_label("\nOK\n");
  g_signal_connect(ok_button, "clicked", G_CALLBACK(ok_dlg), NULL);
  
  GtkButton *close_button=(GtkButton *)gtk_button_new_with_label("Cancel");
  g_signal_connect(close_button, "clicked", G_CALLBACK(close_dlg), NULL);

  GtkBox *box = (GtkBox *)gtk_box_new(GTK_ORIENTATION_VERTICAL, 6);

  GtkGrid *grid = (GtkGrid *)gtk_grid_new();
  gtk_grid_attach(grid, (GtkWidget *)inLabel, 0, 0, 1, 1);
  gtk_grid_attach_next_to(grid, (GtkWidget *)targetLabel, (GtkWidget *)inLabel, GTK_POS_BOTTOM, 1,1);
  gtk_grid_attach_next_to(grid, (GtkWidget *)inLabelName, (GtkWidget *)inLabel, GTK_POS_RIGHT, 1,1);
  gtk_grid_attach_next_to(grid, (GtkWidget *)targetLabelName, (GtkWidget *)targetLabel, GTK_POS_RIGHT, 1,1);
  gtk_grid_attach_next_to(grid, (GtkWidget *)choose_button, (GtkWidget *)targetLabelName, GTK_POS_BOTTOM, 1,1);
  gtk_grid_attach_next_to(grid, (GtkWidget *)close_button, (GtkWidget *)targetLabel, GTK_POS_BOTTOM, 1,3);
  gtk_grid_attach_next_to(grid, (GtkWidget *)ok_button, (GtkWidget *)choose_button, GTK_POS_BOTTOM, 1,2);
  gtk_grid_set_column_spacing(grid, 10);

  gtk_container_add((GtkContainer *)window, (GtkWidget *)box);
  gtk_box_pack_start(box, (GtkWidget *)grid, TRUE, TRUE, 0);
  gtk_widget_grab_focus((GtkWidget *)ok_button);

  gtk_widget_show_all(window);    
}

#! /bin/env pypy3

# a terrible script to help me categorize images from mcomix

import gi
import subprocess # to execute appropriate scripts/programs
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject, GLib, Gio

import os # for os.environ to get DISPLAY variable
import sys # sys.exit
import pathlib # for mkdir

import re # regexes, yay

# btnsize="32" # change this to change the size of button icons.

APP_NAME="Categorize"

# Persist file contains last directory/category targetted.
HOME=os.environ.get("HOME")
rootpath=HOME + "/root"
pathlib.Path(HOME + "/.config/").mkdir(parents=True, exist_ok=True)
pathlib.Path(rootpath).mkdir(parents=True, exist_ok=True)
print("rootpath: " + rootpath)
persistFileName=HOME + "/.config/categorize"

if(not os.path.exists(persistFileName)):
    persistFile=open(persistFileName, 'w')
else:
    persistFile=open(persistFileName, 'r+')

# read back last used path/category from config file (persistFileName).
category=persistFile.readline()
if(category=='' or category=='\n'):
    print("Previous session likely corrupt; erasing contents and using defaults.")
    persistFile.close()
    persistFile=open(persistFileName, 'w')
    category=rootpath
else:
    # Remove trailing newlines if present.                              
    # Yeah, I went a little overboard.                                  
    category=category.rstrip("\r\n")
    category=category.rstrip("\n\r")
    category=category.rstrip("\n")
    category=category.rstrip("\r")
    print("read category path:     "+category+" from file.")

IN_FILE=''
if(len(sys.argv) > 1):
   IN_FILE=sys.argv[1]
else:
   print("Error: No input file name given. I need something to link to!")
   print("(Please give a full path to the file, too.)")
   sys.exit(1)

class CategorizeMain(Gtk.Window):
    inLabel=Gtk.Label(label="File: ")
    inLabel.set_justify(Gtk.Justification.RIGHT)
    inLabel.set_halign(Gtk.Align.END)
    inLabelName=Gtk.Label(label=IN_FILE)
    inLabelName.set_justify(Gtk.Justification.LEFT)
    inLabelName.set_halign(Gtk.Align.END)
    targetLabel=Gtk.Label(label="Category: ")
    targetLabel.set_justify(Gtk.Justification.RIGHT)
    targetLabel.set_halign(Gtk.Align.END)
    targetLabelName=Gtk.Label(label="")
    targetLabelName.set_justify(Gtk.Justification.LEFT)
    targetLabelName.set_halign(Gtk.Align.END)

    dialog=Gtk.FileChooserDialog(title="Select a folder", parent=None,
                                 action=Gtk.FileChooserAction.CREATE_FOLDER)
    dialog.add_buttons("_Cancel", Gtk.ResponseType.CANCEL)
    dialog.add_buttons("_Select", Gtk.ResponseType.OK)
    dialog.set_default_size(400, 200)
    # /home/user/root default
    print("rootpath: " + rootpath)
    Gtk.FileChooser.set_current_folder(dialog, rootpath)
    defaultpath=Gio.file_parse_name(rootpath)
    Gtk.FileChooser.set_current_folder_file(dialog, defaultpath)
    targetLabelName.set_label(category)
    
    def __init__(self):
        inLabel=self.inLabel
        inLabelName=self.inLabelName
        targetLabel=self.targetLabel
        targetLabelName=self.targetLabelName
        dialog=self.dialog

        Gtk.Window.__init__(self, title="Categorize")
        self.set_wmclass(APP_NAME,APP_NAME)

        vbox=Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        chooseButton=Gtk.Button(label="Choose category")
        chooseButton.set_tooltip_text("Select a category folder")
        chooseButton.connect("clicked", self.chooseDlg)
        okButton=Gtk.Button(label="\nOK\n")
        okButton.connect("clicked", self.okDlg)
        closeButton=Gtk.Button(label="Cancel")
        closeButton.connect("clicked", self.closeDlg)
        # not sure spacing actually did anything useful here.
        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6);
        # pack the grid
        grid=Gtk.Grid()

        grid.attach(inLabel, 0,0,1,1)
        grid.attach_next_to(targetLabel, inLabel, Gtk.PositionType.BOTTOM,1,1)
        grid.attach_next_to(inLabelName, inLabel, Gtk.PositionType.RIGHT, 1,1)
        grid.attach_next_to(targetLabelName, targetLabel, Gtk.PositionType.RIGHT, 1,1)
        grid.attach_next_to(chooseButton, targetLabelName, Gtk.PositionType.BOTTOM,1,1)
        grid.attach_next_to(closeButton, targetLabel, Gtk.PositionType.BOTTOM,1,3)
        grid.attach_next_to(okButton, chooseButton, Gtk.PositionType.BOTTOM, 1, 2)
#        grid.set_row_spacing(10)
        grid.set_column_spacing(10)
        self.add(box)
        box.pack_start(grid, True, True, 0)
#       make OK the default button
        okButton.grab_focus()

    def chooseDlg(self, widget):
        global category
        dialog=self.dialog
#        dialog=Gtk.FileChooserDialog(title="Select a folder", parent=self,
#                                     action=Gtk.FileChooserAction.CREATE_FOLDER)
        response=dialog.run()
        if response == Gtk.ResponseType.OK:
            print("Select clicked")
            print("Dir selected: " + dialog.get_filename())
#            shortpath=re.sub("^" + rootpath)
            self.targetLabelName.set_label(dialog.get_filename())
            self.category=dialog.get_filename()
        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")
            
#        dialog.destroy() # breaks on re-opening chooser
        dialog.hide()

    def okDlg(self, widget):
        global persistFile
        global category
        global IN_FILE
        cat=self.dialog.get_filename()
        try:
            cat=self.dialog.get_filename()
        except:
            print("No file was entered, leaving last setting alone (1)")
            self.closeDlg(widget)
        if cat is not None:
            category=cat
            if(category != '' and category != '\n'):
                persistFile.close()
                persistFile=open(persistFileName, 'w')
                persistFile.write(category+'\n')
                persistFile.close()
                
                link_basename=Gio.file_parse_name(IN_FILE).get_basename()
                link_fullpath=Gio.file_parse_name(category + '/' + link_basename).get_path()
                subprocess.run(["ln", "-s", IN_FILE, link_fullpath])
                print(">>>> ln -s '" + IN_FILE + "' '" + link_fullpath + "'")
                
                self.closeDlg(widget)
                sys.exit(0)
            else:
                print("No file was entered, leaving last setting alone (2)")
                link_basename=Gio.file_parse_name(IN_FILE).get_basename()
                link_fullpath=Gio.file_parse_name(category + '/' + link_basename).get_path()
                subprocess.run(["ln", "-s", IN_FILE, link_fullpath])
                print(">>>> ln -s '" + IN_FILE + "' '" + link_fullpath + "'")
                self.closeDlg(widget)
        else:
            print("No file was entered, leaving last setting alone (3)")
            link_basename=Gio.file_parse_name(IN_FILE).get_basename()
            link_fullpath=Gio.file_parse_name(category + '/' + link_basename).get_path()
            subprocess.run(["ln", "-s", IN_FILE, link_fullpath])
            print(">>>> ln -s '" + IN_FILE + "' '" + link_fullpath + "'")
            self.closeDlg(widget)

    def closeDlg(self, widget):
        self.destroy()
        Gtk.main_quit(self, widget)

class HoldButton(Gtk.Button):
   # so we can have 'held' events. Not currently used in this program but might be in future.
    __gsignals__ = { 'held' : (GObject.SignalFlags.RUN_LAST, GObject.TYPE_NONE, ()) }
    def __init__(self, label=None, stock=None, use_underline=True):
        Gtk.Button.__init__(self, label=label, stock=stock, use_underline=use_underline)
        self.connect('pressed', HoldButton.h_pressed)
        self.connect('clicked', HoldButton.h_clicked)
        self.timeout_id = None
    def h_clicked(self):
        if self.timeout_id:
            GObject.source_remove(self.timeout_id)
            self.timeout_id = None
        else:
            self.stop_emission_by_name('clicked')
    def h_pressed(self):
        self.timeout_id = GLib.timeout_add(500, HoldButton.h_timeout, self)
    def h_timeout(self):
        self.timeout_id = None
        self.emit('held')
        return False

win=CategorizeMain()
win.connect("destroy", win.closeDlg)
win.show_all()
Gtk.main()

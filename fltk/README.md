# categorize: fltk

This is an FLTK+shell script reimplemenatation of the GTK python program.

I made it primarily for faster startup times, and also to practice using FLTK.

This version uses the FLTK C++ program as a *helper program* to display a nice
GUI, but does the linking in a shell script. So you should call the shell
script while having the location of the helper binary in your `$PATH` variable.

There's also a version of the program I wrote without the shell helper script,
but it has memory bugs right now, so you should not use it until those are
fixed.


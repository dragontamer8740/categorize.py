/* this version is more minimalist in that it offloads most of the string work
   to a shell. Hopefully more robust. */
/* #define DEBUG 1 */
/* from make command, you can add CXXFLAGS defining NO_HISTORY or
   USE_NATIVE_FILE_CHOOSER as desired. */

/* NOTE: to add keybindings, I should read
   https://www.fltk.org/doc-1.3/subclassing.html which explains
   how to create a class that inherits FL_Choice (or any other FL_Widget)
   that I can override the handle() method of. If I do it right, I think
   I just have to make mine an exact clone of FL_Choice except for the
   overridden handle() method, so i can just replace all other instances of
   FL_Choice in the code and it should work. */



#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
extern char **environ;
#include <libgen.h>
#include <fcntl.h>
#include <time.h>

#include <FL/Fl_Button.H>
#include <FL/Fl_Window.H>

/* for adding new fonts */
#include <FL/fl_draw.H>
#include <FL/Fl_Group.H>
#include <FL/fl_utf8.h>

#if !defined(HISTORY_LINES)
#define HISTORY_LINES 10
#endif

/* move vs rename checkbtn */

/* for alert/warning message boxes */
#include <FL/fl_ask.H>

/* if defined, use the "native" file chooser. If undefined, FLTK built-in. */
/* define USE_NATIVE_FILE_CHOOSER 1 */

#ifdef USE_NATIVE_FILE_CHOOSER
#include <FL/Fl_Native_File_Chooser.H>
#else
#include <FL/Fl_File_Chooser.H>
#endif


#ifndef NO_HISTORY
#include <FL/Fl_Double_Window.H>
/*#include <FL/Fl_Input_Choice.H>*/
#include <FL/Fl_Choice.H>
void choice_cb(Fl_Widget *w, void *userdata);
char *get_line(FILE *stream, int lineno);
char skip_line(FILE *stream);
void populate_history();
char *history_line[HISTORY_LINES];
char *history_label[HISTORY_LINES];
void history_insert(char *str);
int history_find(char *str);
char *truncate_path(char *str);
int count_occurrences(char *str, char search_chr);
int escape_str_calc(char *str, char to_escape);
char *escapify_str(char *in_str, char to_escape); /* this malloc()'s!! */

int history_find(char *str) {
  int i=0;
  while(i<HISTORY_LINES && history_line[i] != NULL) {
    if(!strcmp((const char*)str,(const char*)history_line[i])) {
      return i;
    }
    i++;
  }
  return -1; /* not found */
}



void history_insert(char *str) {
  /* if str is already on the history list, move it
     to the top and push the rest down.
     if it's [3] right now for instance, then put it in a temp spot, and
     work down from 3 - that is, work on [2], [1], [0]. move each to one
     index higher. then make your temp one into index 0.
     if it's not, shift the list down, discarding [4] if applicable,
     and insert it. */
  /* history_find() returns -1 when not found, else the index it was
     found at */
  int bump_index=-1;
  /* char *temp;*/
  if(history_line[0]==NULL) {
    /* the file is totally empty, so we have to just stick it in the
       first slot. */
#ifdef DEBUG
    fprintf(stderr,"FIRST LINE WAS NULL!\n");
#endif
    history_line[0]=str;
    return;
  }
  int idx=history_find(str);
  if(idx==0) {
#ifdef DEBUG
    fprintf(stderr,"Already was at top. Doing nothing.\n");
#endif
    return;
  }
  else if(idx > 0) { /* if already in the list, move up */
#ifdef DEBUG
    fprintf(stderr,"Found in history at position %d\n",idx);
#endif
/*    temp=history_line[idx];*/
    bump_index=idx;
  }
  else {
    /* shift down and bump something off the list to make room if
       necessary */
    /* might need to be doing a malloc and strcpy here? */
    int i=0;
    while(i<HISTORY_LINES && history_line[i] != NULL) {
      i++;
    }
    /* now i is index of first null, or HISTORY_LINES-1 */
    if(i==HISTORY_LINES) {
      i--;
    }
    bump_index=i+1;
  }
  int i=bump_index;
#ifdef DEBUG
  fprintf(stderr,"BUMP_INDEX %d\n",bump_index);
  fprintf(stderr,"TRYING TO INSERT STRING %s\n",str);
#endif
  while(i>0) {
    i--;
#ifdef DEBUG
    fprintf(stderr,"history_line[%d]=history_line[%d]=%s\n",i+1,i,history_line[i]);
#endif
    history_line[i+1]=history_line[i];
  }
/*  if(idx>=0) {
    fprintf(stderr,"history_line[%d]=%s\n",idx,temp);
    history_line[idx]=temp;
  }*/
#ifdef DEBUG
  fprintf(stderr,"history_line[0]=str=%s\n",str);
#endif
  history_line[0]=str;
  
}

/* brute force, make it global. can't make callbacks do what they are supposed to do */
Fl_Choice *history;

#endif



#define WIN_WIDTH 420
#define BASE_WIN_HEIGHT 95

#ifdef NO_HISTORY
#define HISTORY_BOX_HEIGHT 0
#else
#define HISTORY_BOX_HEIGHT 20
#endif

#define WIN_HEIGHT BASE_WIN_HEIGHT + HISTORY_BOX_HEIGHT

#define LABEL_HEIGHT 13
#define LABEL_WIDTH 60
#define RIGHT_COL_WIDTH 360
#define LEFT_COL_WIDTH 50
#define CATEGORY_BTN_HEIGHT 47
#define CHOOSE_BTN_HEIGHT 21

#define LABEL_TOP_ROW_Y HISTORY_BOX_HEIGHT
#define LABEL_BOTTOM_ROW_Y (LABEL_TOP_ROW_Y + LABEL_HEIGHT)

#define CANCEL_BTN_HEIGHT (CATEGORY_BTN_HEIGHT + CHOOSE_BTN_HEIGHT + 1)

/* cancel: 50,66 */
#define CATEGORY_BTN_Y (HISTORY_BOX_HEIGHT + (LABEL_HEIGHT * 2) + 1)
#define CANCEL_BTN_Y CATEGORY_BTN_Y

#define OK_BTN_Y (HISTORY_BOX_HEIGHT + (LABEL_HEIGHT * 2) + CHOOSE_BTN_HEIGHT + 1)
#define CHOOSE_BTN_Y (HISTORY_BOX_HEIGHT + (LABEL_HEIGHT * 2) + 1)
#define VAR_FIELD_WIDTH (WIN_WIDTH-LABEL_WIDTH)


/*#define CUSTOM_TYPEFACE "Microsoft Sans Serif"*/
         /* MS UI Gothic */
/* font to use in (FLTK) file chooser dialogue: */
#define CUSTOM_TYPEFACE "IBM 3161 Bitmap"
#define CUSTOM_TYPEFACE_SIZE 16
/* font to use in main window: */
#define CUSTOM_TYPEFACE2 "MS UI Gothic"
/* #define CUSTOM_TYPEFACE2 "Noto Sans" */
/* define CUSTOM_TYPEFACE2 "IBM3161" */
#define CUSTOM_TYPEFACE2_SIZE 12

#define ORIG_FL_NORMAL_SIZE FL_NORMAL_SIZE

void font_init(); /* creates a new font 'object' that will be used from then on */
void setup_main_font(); /* sets global font */
void widget_setup(Fl_Widget *widget);
void menu_widget_setup(Fl_Menu_ *widget);
void box_button_setup(Fl_Button *button);
void chooser_setup(Fl_File_Chooser *chooser);
void quit_app_callback(Fl_Widget*, void*);
void do_linking(Fl_Widget*, void*);
void cleanup();
void alert(char *text);

int dir_exists(char *path);
int line_length(FILE *file);
char *persist_file_read(char *path);
int persist_file_write(char *path, char *string);
void do_link(char *origin, char *destpath);

/* these four are all read from environment if possible */
char *target;

char *category_dir; // value read from config_file or chosen in selector
char *category_root_dir; // origin path to start in by default
char *config_file;

#define XDG_CONFIG_PATH_FROM_HOME "/.config"
const char *subdir_root="/root/";
const char *persist_file="/categorize"; /* $XDG_CONFIG_DIR/categorize */
const char *app_name="Categorize"; /* for FVWM to be happy */

Fl_Box *categoryname; /* global so we can change the label */
Fl_Group *rightcol; /* redraw() after changing label */

void initialize_category_dir() {
  config_file=getenv("CATEGORIZE_CONFIG_FILE");
  if(config_file==NULL) {
    /* try our possibly broken mangling function if no variable*/
    config_file=(char *)malloc((strlen(getenv("HOME")) + strlen(XDG_CONFIG_PATH_FROM_HOME) + strlen(persist_file) + 1) * sizeof(char)); /* null terminated */
    strcpy(config_file, getenv("HOME"));
    strcat(config_file, XDG_CONFIG_PATH_FROM_HOME);
    strcat(config_file, persist_file);
    config_file[strlen(getenv("HOME")) + strlen(XDG_CONFIG_PATH_FROM_HOME) + strlen(persist_file)]='\0';
  }
#ifdef DEBUG
  fprintf(stderr,"config file is %s\n",config_file);
#endif
  
  /* category root dir */
  category_root_dir=getenv("CATEGORIZE_CATEGORY_ROOT_DIR");
  if(category_root_dir==NULL) {
    /*
      by default, the file chooser will have the filename field text highlighted
      unless there's a trailing slash on the path. So we have to malloc one
      additional character (thus the + sizeof(char)). strlen("/") would also
      work, but probably takes more cycles.
    */
    category_root_dir=(char *)malloc(((strlen(getenv("HOME")) + strlen(subdir_root)) + 1)* sizeof(char)); /* null terminated */
    strcpy(category_root_dir, getenv("HOME"));
    strcat(category_root_dir, subdir_root);
    category_root_dir[strlen(getenv("HOME")) + strlen(subdir_root)]='\0';

  }
#ifdef DEBUG
  fprintf(stderr,"category root dir is %s\n",category_root_dir);
#endif
  /* persist file path */

  category_dir=getenv("CATEGORIZE_CATEGORY_DIR");
  if(category_dir==NULL) {
    /* category dir: read from file, or if absent malloc and strcpy from
       category root dir */
    FILE *persist;
    if ((persist = fopen(config_file, "a+b"))) /* create if missing */
    {
      fseek(persist, 0, SEEK_SET); /* some libc's start at end of file. so seek back. */
      int len=line_length(persist);
      /* seek back again after reading */
      fseek(persist, 0, SEEK_SET);
      if(len==0) {
        /* file was empty or didn't exist, so start out in category_root_dir this time around */
        category_dir=(char *)malloc( (strlen(category_root_dir) + 1) * sizeof(char));
        strcpy(category_dir,category_root_dir);
      }
      else {
        /* allocate length and read string into memory */
        category_dir=(char *)malloc( (len + 1) * sizeof(char));
        /* read that line into memory, sans newline character */
        fgets(category_dir, len + 1, persist);
#ifdef DEBUG
        fprintf(stderr, "%s Opened successfully\n",config_file);
#endif
      }
      fclose(persist);
    }
    else
    {
#ifdef DEBUG
      fprintf(stderr, "E: Could not create config file!\n");
#endif
    }
#ifdef DEBUG
    fprintf(stderr,"Category dir is %s\n",category_dir);
#endif
  }
}


char *truncate_path(char *str) {
  /* returns a pointer to the place in the string one past the last slash,
     but only if the path leading up to that was category_root_dir */
  if(!strncmp(str,category_root_dir,strlen(category_root_dir))) {
    /* if strings match */
#ifdef DEBUG
    fprintf(stderr,"%s\n", str+strlen(category_root_dir));
#endif
    return str+strlen(category_root_dir);
  }
  return str; /* return original string otherwise */
}


#ifdef USE_NATIVE_FILE_CHOOSER
void fileChosen(char *path) {
  /* category_dir gets a malloc from initialize_category_dir() which should
     already have run. So we can do reallocs. */
  if(strlen(path) > 0) {
#ifdef DEBUG
    fprintf(stderr,"fixme: I am almost certain that this place in code is causing a memory leak, but I need to figure out why it causes double free errors then\n");
#endif
    /* trim off trailing slashes, if present. Just for de-duping. */
    size_t copylength=strlen(path);
    if(path[strlen(path)-1] == '/' && strlen(path) > 1) {
      /* cut out the end slash */
      copylength--;
    }
    /* REALLOC */
    category_dir=(char *)realloc(category_dir,(copylength+1) * sizeof(char));
    if(category_dir == NULL) {
      fprintf(stderr, "ERROR: Failed to realloc() category_dir\n");
    }
    strncpy(category_dir, path, copylength);
    category_dir[copylength]='\0';

    

    
#ifdef DEBUG
    fprintf(stderr,"Category dir is %s\n", category_dir);
#endif
/*#ifndef NO_HISTORY
    history->label("");
    #endif*/
    categoryname->label(category_dir);
    rightcol->redraw();
  }
}
#else
/* fltk-based chooser */
void fileChosen(Fl_File_Chooser* fileChooser, void*) {
#ifdef DEBUG
  fprintf(stderr,"is shown: %d\n", fileChooser->shown());
#endif
  if (!fileChooser->shown()) {
    if(strlen(fileChooser->value(1)) > 0) {
#ifdef DEBUG
      fprintf(stderr,"fixme: I am almost certain that this place in code is causing a memory leak, but I need to figure out why it causes double free errors then\n");
#endif
//(strikeout)THIS IS ALMOST DEFINITELY A MEMORY LEAK BUT I AM DEBUGGING
// I think it is fixed now.
//      free(category_dir);
      size_t copylength=strlen(fileChooser->value(1));
      if(fileChooser->value(1)[strlen(fileChooser->value(1))-1] == '/' && strlen(fileChooser->value(1)) > 1) {
        /* cut out the end slash */
        copylength--;
      }
      /* REALLOC */
      category_dir=(char *)realloc(category_dir,(copylength+1) * sizeof(char));
      if(category_dir == NULL) {
        fprintf(stderr, "ERROR: Failed to realloc() category_dir\n");
      }
      strncpy(category_dir, fileChooser->value(1), copylength);
      category_dir[copylength]='\0';

      /*
      category_dir=(char*)malloc((strlen(fileChooser->value(1)) + 1) * sizeof(char));
      strcpy(category_dir,fileChooser->value(1));*/
#ifdef DEBUG
      fprintf(stderr,"Category dir is %s\n",category_dir);
#endif
/*#ifndef NO_HISTORY
      history->label("");
      #endif*/
      categoryname->label(category_dir);
      rightcol->redraw();
    }
    delete fileChooser;
  }
}

#endif

/* callback to quit */
void quit_app_callback(Fl_Widget*, void*) {
  cleanup();
  exit(2);
}

#ifdef USE_NATIVE_FILE_CHOOSER
/* "native" picker */
void showFileChooser(Fl_Widget*, void*) {

  Fl_Native_File_Chooser fileChooser;
  fileChooser.title("Select a Directory");
  fileChooser.type(Fl_Native_File_Chooser::BROWSE_DIRECTORY);
  fileChooser.directory(category_root_dir); 
  // Show file chooser
  switch (fileChooser.show()) {
  case -1: fprintf(stderr, "Error: %s\n", fileChooser.errmsg()); break;
  case 1: break; // Cancel
  default:
    if (fileChooser.filename()) {
      fileChosen((char*)fileChooser.filename());
                  }
    break;
  }
}


  
/*  Fl_Native_File_Chooser* fileChooser = new Fl_Native_File_Chooser(category_root_dir, "*.*", Fl_File_Chooser::DIRECTORY, "Choose directory");
  fileChooser->sort=fl_casealphasort;
  fileChooser->preview(0);
  fileChooser->callback(fileChosen);
  fileChooser->show();
  }*/
#else
/* need to make a native one instead to get unicode */
void showFileChooser(Fl_Widget*, void*) {
  Fl_File_Chooser* fileChooser = new Fl_File_Chooser(category_root_dir, "*.*", Fl_File_Chooser::DIRECTORY, "Choose directory");
  fileChooser->sort=fl_casealphasort;
/*  fileChooser->preview(0); */
  /* these next two calls seem to BOTH be necessary to make it not highlight
     the directory path in the text input field. */
  fileChooser->value("");
  fileChooser->directory(category_root_dir);
  fileChooser->callback(fileChosen);
  fileChooser->show();
}
#endif

#if defined(CUSTOM_TYPEFACE)
void setup_main_font() {
  /* this is not especially necessary on FLTK 1.4 (if built with
     --enable-pango), but is vital on 1.3.x for unicode text to render
     since the given font needs to have every required glyph in it. */
  /* FL_NORMAL_SIZE=ORIG_FL_NORMAL_SIZE; */
  FL_NORMAL_SIZE=CUSTOM_TYPEFACE_SIZE;
  Fl::set_font(FL_HELVETICA, CUSTOM_TYPEFACE );
  Fl::set_font(FL_HELVETICA_BOLD, CUSTOM_TYPEFACE );
  Fl::set_font(FL_HELVETICA_ITALIC, CUSTOM_TYPEFACE );
  Fl::set_font(FL_HELVETICA_BOLD_ITALIC, CUSTOM_TYPEFACE );
  return;
}
#endif

int main(int argc, char* argv[]) {  
  if(argc > 1) {
    target=argv[1];
  }
  else
  {
    target=NULL;
  }

/* To center window on screen, need to get info on window & screen first. */
  int screen_x, screen_y, screen_w, screen_h;
/* Uncomment this line and comment the one below if you do care about
   working area: */
/*Fl::screen_work_area(&screen_x, &screen_y, &screen_w, &screen_h);*/
  Fl::screen_xywh(screen_x, screen_y, screen_w, screen_h);
  
#if defined(CUSTOM_TYPEFACE)
  setup_main_font();
#endif
  /* Fl::get_system_colors(); */
  /* we are going to force the 'none' style to look more like my Motif/CDE
     desktop */
  Fl::background(174,178,195);
  Fl::background2(255,247,233);
  /* calculate category dirs and allocate/assign them
    (global vars *category_dir and *category_root_dir) */
  initialize_category_dir();
#ifndef NO_HISTORY
  /* load history entries */
  populate_history();
#endif
  /* construct the main window */

  Fl_Window *window = new Fl_Window(WIN_WIDTH,WIN_HEIGHT);
  window->xclass("categorize_mcomix");
  window->callback(quit_app_callback);

  font_init();
  /* handle custom font creation (see preprocessor defines up top) */
#ifndef NO_HISTORY
/*  Fl_Group *big_grp = new Fl_Group(0,0,420,122);*/
#endif
  Fl_Group *grp = new Fl_Group(0,HISTORY_BOX_HEIGHT,WIN_WIDTH,WIN_HEIGHT);






  rightcol = new Fl_Group(LABEL_WIDTH, HISTORY_BOX_HEIGHT, VAR_FIELD_WIDTH, WIN_HEIGHT); // y=52 for start of choose category button i think
  Fl_Box *filename = new Fl_Box(LABEL_WIDTH, HISTORY_BOX_HEIGHT, 0, LABEL_HEIGHT );
  /* categoryname is global for convenience */
  categoryname = new Fl_Box(LABEL_WIDTH, LABEL_BOTTOM_ROW_Y, 0, LABEL_HEIGHT );
  widget_setup((Fl_Widget *)filename);
  widget_setup((Fl_Widget *)categoryname);
  if(argc > 0) {
    filename->label(argv[1]);
  }
  else {
    filename->label("NONE!!!!!!");
  }
  categoryname->label(category_dir);
  filename->align(FL_ALIGN_RIGHT);
  categoryname->align(FL_ALIGN_RIGHT);
  Fl_Button *choosebutton = new Fl_Button(LABEL_WIDTH, CATEGORY_BTN_Y, VAR_FIELD_WIDTH, CHOOSE_BTN_HEIGHT, "Choose Category (&9)");
  /* press enter and this widget intercepts it, no matter what keyboard focus is */
  Fl_Button *okbutton = new Fl_Return_Button(LABEL_WIDTH, OK_BTN_Y, VAR_FIELD_WIDTH, CATEGORY_BTN_HEIGHT, "OK");
    /* Fl_Button *okbutton = new Fl_Button(60, 47, 360, 47, "OK"); */
  box_button_setup(choosebutton);
  box_button_setup(okbutton);
  okbutton->clear_visible_focus();
  rightcol->end();


  Fl_Group *leftcol = new Fl_Group(0,HISTORY_BOX_HEIGHT,LEFT_COL_WIDTH,WIN_HEIGHT);
  Fl_Box *filelabel = new Fl_Box(0, LABEL_TOP_ROW_Y, 0, LABEL_HEIGHT );
  Fl_Box *categorylabel = new Fl_Box(0, LABEL_BOTTOM_ROW_Y, 0, LABEL_HEIGHT );
  widget_setup((Fl_Widget *)filelabel);
  widget_setup((Fl_Widget *)categorylabel);
  filelabel->label("File:");
  categorylabel->label("Category:");
  filelabel->align(FL_ALIGN_RIGHT);
  categorylabel->align(FL_ALIGN_RIGHT);
  Fl_Button *cancelbutton = new Fl_Button(0, CATEGORY_BTN_Y, LEFT_COL_WIDTH, CANCEL_BTN_HEIGHT, "Cancel");
  box_button_setup(cancelbutton);
  cancelbutton->clear_visible_focus();
  leftcol->end();

  
#ifndef NO_HISTORY

#endif
  rightcol->resizable(okbutton);
  leftcol->resizable(cancelbutton);
  
  grp->end();
#ifndef NO_HISTORY
//  history->resizable(grp);
/*  big_grp->end();
    big_grp->*/
  window->resizable(grp);
#else
  window->resizable(grp);
#endif
  if(argc > 2) {
     /* last argument becomes window title */
    window->label(argv[2]);
  }
  else {
    window->label("Categorize");
  }
  cancelbutton->callback(quit_app_callback);
  choosebutton->callback(showFileChooser);
  choosebutton->clear_visible_focus();
  /* 'B'
  choosebutton->shortcut(0x62);*/
  /* '/' */
  /*choosebutton->shortcut(0x2f);*/
  /* Right Shift */
  /* choosebutton->shortcut(0xffe2);*/
  /* '9' */
  choosebutton->shortcut(0x39);
  okbutton->callback(do_linking);
#ifdef NO_HISTORY
  choosebutton->take_focus(); /* when widget starts, space will open chooser, enter will do the "ok" function, escape will quit. */
  /* if NO_HISTORY was not defined, then the history Fl_Choice will be the focus. */
#else
  /* if NO_HISTORY is not defined, then we want to show the Fl_Choice widget at the top. */
  Fl_Choice *history=new Fl_Choice(0,0,WIN_WIDTH,HISTORY_BOX_HEIGHT,NULL);
  menu_widget_setup((Fl_Menu_ *)history);
  history->type(Fl_Menu_Button::POPUP123);
  history->callback(choice_cb, 0);
/*  history->menubutton()->shortcut(0xff54);*/
/*  history->take_focus();*/
  Fl::focus(history);
  int i=0;
  while(i<HISTORY_LINES && history_line[i] != NULL) {
#ifdef DEBUG
    fprintf(stderr,"Adding line %d to box\n",i);
#endif /* DEBUG */
#if !defined(LONG_PATHS)
    char *trunc=truncate_path(history_line[i]);
    /* if starts with prefix dir, we can strip that. */
    /* escapify_str() does a malloc */
    char *tmp=history_label[i]; /* don't want to lose this */
    /* escapify_str() does a malloc and copy */
    history_label[i]=escapify_str(trunc, '/');
    free(tmp); /* free old label */
    
    /* DO NOT free trunc; we'd want to free history_line[i] instead since trunc
       points to a location inside of history_line[i] */
    /* history->add(trunc,0,0,(void*)trunc); */
/*    history->add(history_label[i],0,0,(void*)history_label[i]);*/
#endif
/* #else */
    /* show full path in the Fl_Choice. */
    /* history->add(history_line[i],0,0,(void*)history_line[i]); */
    history->add(history_label[i],0,0,(void*)history_label[i]);
    /* !LONG_PATHS */
/* #endif */
    i++;
  }
  if(history_line[0] != NULL) {
    history->value(0);
 } 
#endif /* NO_HISTORY */
  window->end();
  
  int dec_width  = window->decorated_w();
  int dec_height = window->decorated_h();

/*  int win_init_x = ((screen_w - dec_width) / 2);
    int win_init_y = ((screen_h - dec_height) / 2);*/
  int win_init_x=(screen_w - dec_width-5);
  int win_init_y=(screen_h-dec_height-16);
  
  window->position(win_init_x,win_init_y);

/*  window->show(argc, argv);*/
  window->show();

//  history->menubutton()->popup();
  
/* show() without those arguments lets us use our own */
  return Fl::run();
}

void widget_setup(Fl_Widget *widget)
{
#if defined(CUSTOM_TYPEFACE_SIZE)
  fl_font(FL_FREE_FONT, CUSTOM_TYPEFACE_SIZE);
#else
  fl_font(FL_FREE_FONT, 11); /* default to 11px */
#endif /* defined(CUSTOM_TYPEFACE2_SIZE) */
  widget->labelfont(fl_font());
  /* widget->labelfont(FL_BOLD+FL_ITALIC); */
#if defined(CUSTOM_TYPEFACE2_SIZE)
  widget->labelsize(CUSTOM_TYPEFACE2_SIZE);
#else
  widget->labelsize(11);
#endif
}

void menu_widget_setup(Fl_Menu_ *widget) {
#if defined(CUSTOM_TYPEFACE_SIZE)
  fl_font(FL_FREE_FONT, CUSTOM_TYPEFACE_SIZE);
#else
  fl_font(FL_FREE_FONT, 11); /* default to 11px */
#endif /* defined(CUSTOM_TYPEFACE2_SIZE) */
  widget->textfont(fl_font());
  /* widget->labelfont(FL_BOLD+FL_ITALIC); */
#if defined(CUSTOM_TYPEFACE2_SIZE)
  widget->textsize(CUSTOM_TYPEFACE2_SIZE);
#else
  widget->textsize(11);
#endif
}

void font_init()
{
  /* YUCK. todo: Fix this ugly preprocessor garbage for font selection
   at compile-time. */
#if defined(CUSTOM_TYPEFACE2)
  Fl::set_font(FL_FREE_FONT, CUSTOM_TYPEFACE2);
#if defined(CUSTOM_TYPEFACE_SIZE)
  fl_font(FL_FREE_FONT, CUSTOM_TYPEFACE_SIZE);
#else
  fl_font(FL_FREE_FONT, 11); /* default to 11px */
#endif /* defined(CUSTOM_TYPEFACE2_SIZE) */
#else /* default to FL_HELVETICA if no custom font is defined */
#if defined(CUSTOM_TYPEFACE_SIZE)
  fl_font(FL_HELVETICA, CUSTOM_TYPEFACE_SIZE);
#else
  fl_font(FL_HELVETICA, 11); /* default to 11px */
#endif /* defined(CUSTOM_TYPEFACE2_SIZE) */
#endif /* defined(CUSTOM_TYPEFACE) */
}

void box_button_setup(Fl_Button *button)
{
  /* button->button(FL_UP_BOX); */
  widget_setup((Fl_Widget *) button);
}


/* callback to do everything */
void do_linking(Fl_Widget*, void*){
  if(target) {
    /* target isn't null: an argument was actually passed to the program */
#ifdef DEBUG
    fprintf(stderr,"persist_file_write(%s, %s)",config_file, category_dir);
#endif
/*    persist_file_write(config_file, category_dir); */
    do_link(target, category_dir);
    /* now quit */
    cleanup();
    exit(0);
  }
  else {
    fprintf(stderr,"E: No file was chosen.\n");
    alert((char *)"Warning: No input file was given as an argument!\n\nNothing done..");
  }
  return;
}

int line_length(FILE *file)
{
  char a;
  int i=0;
  a=getc(file); /* length 0 means empty file */
  while(a != '\n' && a != EOF)
  {
    a=getc(file);
    i++;
  }
  return i;
}


char *persist_file_read(char *path)
{
  FILE *persist_file=fopen(path, "rb"); /* 'b' is for windows */
  char *string;
  if(persist_file==NULL)
  {
    return NULL;
  }
  else
  {
    /* get first line length sans newline */
    int pathlen=line_length(persist_file);
    /* seek to beginning of file again */
    fseek(persist_file, 0, SEEK_SET);
    string=(char *)malloc((pathlen + 1) * sizeof(char));
    /* read that line into memory, sans newline character */
    fgets(string, pathlen + 1, persist_file);
#ifdef DEBUG
    fprintf(stderr,"%s Opened successfully\n",path);
#endif
    fclose(persist_file);
  }
  return string;
}

int persist_file_write(char *path, char *string)
{

  FILE *persist_file=fopen(path, "wb");
  if(persist_file==NULL)
  {
    /* error */
    fprintf(stderr,"Error opening persistence file to write on exit.\n");
    return -1;
  }
#ifdef NO_HISTORY
  fprintf(persist_file,"%s\n",string);
#else
#ifdef DEBUG
  fprintf(stderr,"history_insert(\"%s\");\n",category_dir);
#endif
  history_insert(category_dir);
  int i=0;
  while(history_line[i] != NULL && i < HISTORY_LINES) {
#ifdef DEBUG
    fprintf(stderr,"persist_file_write(): Writing \"%s\" to persist file\n",history_line[i]);
#endif
    fprintf(persist_file,"%s\n",history_line[i]);
    i++;
  }
#endif
  fclose(persist_file);
  return 0;
}

void cleanup() {
/*  free(category_dir);*/ /* not malloc'd by malloc, mayhaps? */
  free(category_root_dir);
  free(config_file);
  return;
}

void do_link(char *origin, char *destpath){
#ifndef DO_NATIVE_LINKING
  printf("%s\n",destpath); /* let shell script append for us */
}

#else
  /* native linking, probably has a memory bug */
  /* first get basename of origin */
  char *origin_base_name;
  char *working_buf;
  char *to_free;
  char *combined_destination;
  /* laziness here: we just allocate an equal amount of space as the source string since that should always be enough. */
  working_buf=(char*)malloc((strlen(origin) + 1) * sizeof(char));
  to_free=working_buf;
  /* basename() can modify original string; therefore we do a copy first: */
  strcpy(working_buf, origin);
  working_buf=basename(working_buf);
  origin_base_name=(char*)malloc((strlen(working_buf) + 1) * sizeof(char));
  strcpy(origin_base_name,working_buf);
  free(to_free);

  /* now we have one allocated block of memory: the basename'd string. */
  /* the to_free and working_buf pointers are not valid at this point. */
  /* next we need to concatenate it to destpath. */
  combined_destination=(char*)malloc( ( sizeof(origin_base_name) + sizeof(destpath) + 2) * sizeof(char)); /* +2 because '/' and terminator */
  strcpy(combined_destination, destpath);
  strcat(combined_destination, "/");
  strcat(combined_destination, origin_base_name);
  category_root_dir[strlen(getenv("HOME")) + strlen(subdir_root)]='\0';
  free(origin_base_name);

  /* now we have our combined string: [destpath]/[origin_base_name], and
     once more have one allocated block: combined_destination. 
     Next, we need to make the link. */
#ifdef DEBUG
  fprintf(stderr,"LINKING: %s -> %s\n",origin,combined_destination);
#endif
  
  /* *ON LINUX*, and *POSSIBLY ONLY ON LINUX,* we can use stat() to check
     the st_dev field and see if two files are on the same device (and
     determine whether to hard or soft link). See stat(2). It looks like
     at least OpenBSD also provides st_dev, though. */
  struct stat *origin_stats=(struct stat *)malloc(sizeof(struct stat));
  struct stat *dest_stats=(struct stat *)malloc(sizeof(struct stat));
  struct stat *combined_destination_stats=(struct stat *)malloc(sizeof(struct stat));
  int check;
  lstat(combined_destination, combined_destination_stats); /* the destination */
  if(combined_destination_stats != NULL) {
    /* check if symbolic link, remove it if it is. */
    if ((combined_destination_stats->st_mode & S_IFMT)==S_IFLNK) {
      /* I was hitting some weird sort of race condition, so this slows the program down a wee bit to avoid a glitch with unlink(). */
      struct timespec TS;
      /* reminder: 0.1 seconds is 100,000,000 nS */
      TS.tv_sec=(time_t)0;
      TS.tv_nsec=(long)10000;
      nanosleep(&TS, NULL);
      check=unlink(combined_destination);
      if(check != 0) {
#ifdef DEBUG
        fprintf(stderr,"E: Something happened wrong when trying to remove symbolic link at %s. errno %d\n",combined_destination,errno);
#endif
      }
      else {
#ifdef DEBUG
        fprintf(stderr,"Removed existing link to make room for new file.\n");
#endif
      }
    }
  }
  /* if it was a real file, we'll catch that in a moment. */
  free(combined_destination_stats);
  
  if(origin_stats == NULL || dest_stats == NULL) {
    fprintf(stderr,"E: malloc() failed!\n");
    return;
  }
/* lstat doesn't follow symlinks. */
  lstat(origin, origin_stats); /* the original file */
  lstat(destpath, dest_stats); /* the directory to contain our new link */
  if(origin_stats == NULL || dest_stats == NULL) {
    fprintf(stderr, "E: something went wrong performing lstat().\n");
  }
  
  printf("DEBUG: st_dev: %ld vs. %ld\n",origin_stats->st_dev,dest_stats->st_dev);
  if(origin_stats->st_dev != dest_stats->st_dev) {
    /* we need to do a symbolic link to bridge partitions. */
    /* first we need to check if a symlink at the destination already exists,
       since the function doesn't let us force overwrite links. */
    fprintf(stderr,"Source and destination are on different partitions. Symbolic linking.\n");
    check=symlink(origin, combined_destination);
    if(check != 0) {
      fprintf(stderr, "ERROR: symbolic link creation failed...\n");
    }
  }
  else {
    fprintf(stderr,"Source and destination are on the same partition. Hard linking.\n");
    /* note: todo: we currently are hardlinking to symbolic links. Might want to
       follow these in the future and then check if they're on the same device. */
    /*linkat is stupid and requires opening fd's before it will work even though
      i don't want to follow relative links. But posix link(3posix) manpage says this
      should work.  */
    check=linkat(AT_FDCWD, origin, AT_FDCWD, combined_destination, AT_SYMLINK_FOLLOW);
    if(check!=0) {
      fprintf(stderr, "ERROR: hard link creation failed...\n");
    }
  }
  fprintf(stderr,"Link attempted. Check if it worked.\n");  
  free(origin_stats);
  free(dest_stats);
  free(combined_destination);
  /* return to do_linking */
}
#endif

void alert(char *text) {
#if defined(CUSTOM_TYPEFACE2)
    Fl::set_font(FL_HELVETICA, CUSTOM_TYPEFACE2 );
#if defined(CUSTOM_TYPEFACE2_SIZE)
    FL_NORMAL_SIZE=CUSTOM_TYPEFACE2_SIZE;
#endif
#endif
    fl_alert(text);
    /* now revert font choices */
#if defined(CUSTOM_TYPEFACE)
#if defined(CUSTOM_TYPEFACE_SIZE)
    /* set back to size used inside of the file chooser */
    FL_NORMAL_SIZE=CUSTOM_TYPEFACE_SIZE;
#endif
    setup_main_font();
#endif
}

#ifndef NO_HISTORY

// Handle the different menu items..                                                                                                                         
void choice_cb(Fl_Widget *w, void* data) {
  Fl_Choice *choice=(Fl_Choice*)w;
  /* this has no memory handling for freeing stuff */
#ifdef DEBUG
  fprintf(stderr,"%d\n",choice->value());
#endif
  if(choice->value() >= 0) {
    category_dir=history_line[choice->value()];
    categoryname->label(category_dir);
  }
  rightcol->redraw();
}

void populate_history() {
  /* MUST ONLY BE CALLED AFTER CATEGORY_DIR INITIALIZED. */
  /* it sets up the config_file name. */
  FILE *persist;
  if((persist = fopen(config_file, "a+b"))) { /* create if missing */
    fseek(persist,0,SEEK_SET);
  }
  int i=0;
  int len=0;
/*  unsigned int check;*/
  int check;
  char *c=(char *)""; /* hope this is ok to reinitialize later, but it can't be
                 null to start*/
  long last_offset=0;
  char *string;
  while(i<HISTORY_LINES) {
    history_line[i]=NULL; /* start off with known values */
    history_label[i]=NULL;
    i++;
  }
  i=0;
  while(i<HISTORY_LINES && c != NULL) {
    last_offset=ftell(persist); /* mark end of line for next run */
    len=line_length(persist);
    if(len > 0) {
      fseek(persist, last_offset, SEEK_SET);
      string=(char *)malloc((len + 1) * sizeof(char));
      c=fgets(string, len + 1, persist);
#ifdef DEBUG
      fprintf(stderr, "%s\n",string);
#endif
      if((check=fgetc(persist)) != EOF) { /* have to advance past the newline for next run-around */
/*      if(c != NULL) {*/
        /* check to make sure it doesn't exist already at destination */
        if(history_find(string) == -1) {
          history_line[i]=string;
          /* escapify_str() performs a malloc */
          history_label[i]=escapify_str(string, '/');
          i++; /* only increment i when non-duplicate string is added. */
        }
        else {
#ifdef DEBUG
          fprintf(stderr, "Your history file has a duplicated line! Skipping.\n");
#endif
        }
      }
    }
    else {
#ifdef DEBUG     
      fprintf(stderr,"setting c to NULL\n");
#endif
      /* this is the terminating case for when fewer than HISTORY_LINES
         non-duplicate items are found. */
      c=NULL;
    }
  }
  i=0;
#ifdef DEBUG
  while(i<HISTORY_LINES && history_line[i] != NULL) {
    fprintf(stderr, "LINE %d: %s (%s)\n",i, history_line[i], history_label[i]);
    i++;
  }
#endif
}
#endif

char *escapify_str(char *in_str, char to_escape) {
  int siz=escape_str_calc(in_str, to_escape);
  char *result=(char *)malloc(siz);
  size_t i=0;
  size_t dest_i=0;
  while(i<strlen(in_str)) {
    if(in_str[i] == to_escape) {
      result[dest_i]='\\';
      dest_i++;
      result[dest_i]=in_str[i];
    }
    else {
      result[dest_i]=in_str[i];
    }
    dest_i++;
    i++;
  }
  result[siz-1]='\0';
  return result;
}

int count_occurrences(char *str, char search_chr) {
  size_t i=0;
  int found=0;
  while(i<strlen(str)) {
    if(str[i] == search_chr)
      found++;
    i++;
  }
  return found;
}

int escape_str_calc(char *str, char to_escape) {
  return (strlen(str) + 1 + count_occurrences(str, to_escape)) * sizeof(char);
}

/*
int Fl_Return_Button::handle(int event) {
  if (event == FL_SHORTCUT &&
      (Fl::event_key() == FL_Enter || Fl::event_key() == FL_KP_Enter)) {
    simulate_key_action();
    do_callback();
    return 1;
  } else
    return Fl_Button::handle(event);
}
*/


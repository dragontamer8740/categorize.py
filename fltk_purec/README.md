## DO NOT USE

This implementation has memory management bugs in its string handling that I
haven't sat down to figure out yet. Don't use it.

Use the other fltk implementation instead, which uses a shell script wrapper
around the GUI part of this program to minimize the amount of string
management that has to be done in C. In that version, the shell script  also
performs the checks for existing files/links and the actual symbolic linking.

Note to self: Try using realloc() in place of my free's and malloc's. Looks
like it may be a little simpler.

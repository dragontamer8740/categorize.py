#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <libgen.h>
#include <fcntl.h>
#include <time.h>

#include <FL/Fl_Button.H>
#include <FL/Fl_Window.H>

/* for adding new fonts */
#include <FL/fl_draw.H>
#include <FL/Fl_Group.H>
#include <FL/fl_utf8.h>

/* for alert/warning message boxes */
#include <FL/fl_ask.H>

/* if defined, use the "native" file chooser. If undefined, FLTK built-in. */
/* define USE_NATIVE_FILE_CHOOSER 1 */

#ifdef USE_NATIVE_FILE_CHOOSER
#include <FL/Fl_Native_File_Chooser.H>
#else
#include <FL/Fl_File_Chooser.H>
#endif

#define CATEGORY_ROOT "/home/root/"

/*#define CUSTOM_TYPEFACE "Microsoft Sans Serif"*/
         /* MS UI Gothic */
/* font to use in (FLTK) file chooser dialogue: */
#define CUSTOM_TYPEFACE "IBM 3161 Bitmap"
/* font to use in main window: */
#define CUSTOM_TYPEFACE2 "MS UI Gothic"
/* #define CUSTOM_TYPEFACE2 "Noto Sans" */
/* define CUSTOM_TYPEFACE2 "IBM3161" */
#define CUSTOM_TYPEFACE2_SIZE 12
#define CUSTOM_TYPEFACE_SIZE 16

#define ORIG_FL_NORMAL_SIZE FL_NORMAL_SIZE
void font_init(); /* creates a new font 'object' that will be used from then on */
void setup_main_font(); /* sets global font */
void widget_setup(Fl_Widget *widget);
void box_button_setup(Fl_Button *button);
void chooser_setup(Fl_File_Chooser *chooser);
void quit_app_callback(Fl_Widget*, void*);
void do_linking(Fl_Widget*, void*);
void cleanup();
void alert(char *text);

int dir_exists(char *path);
int line_length(FILE *file);
char *persist_file_read(char *path);
int persist_file_write(char *path, char *string);
void do_link(char *origin, char *destpath);

char *target;

char *category_dir; // value read from config_file or chosen in selector
char *category_root_dir; // origin path to start in by default
char *config_file;
#define XDG_CONFIG_PATH_FROM_HOME "/.config"

const char *subdir_root="/root/";
const char *persist_file="/categorize"; /* $XDG_CONFIG_DIR/categorize */
const char *app_name="Categorize"; /* for FVWM to be happy */

Fl_Box *categoryname; /* global so we can change the label */
Fl_Group *rightcol; /* redraw() after changing label */

void initialize_category_dir() {
  /* category root dir */
  /*
    by default, the file chooser will have the filename field text highlighted
    unless there's a trailing slash on the path. So we have to malloc one
    additional character (thus the + sizeof(char)). strlen("/") would also
    work, but probably takes more cycles.
  */
  category_root_dir=(char *)malloc((strlen(getenv("HOME")) + strlen(subdir_root)) +1 * sizeof(char)); /* null terminated */
  strcpy(category_root_dir, getenv("HOME"));
  strcat(category_root_dir, subdir_root);
  category_root_dir[strlen(getenv("HOME")) + strlen(subdir_root)]='\0';
  printf("category root dir is %s\n",category_root_dir);

  /* persist file path */
  config_file=(char *)malloc((strlen(getenv("HOME")) + strlen(XDG_CONFIG_PATH_FROM_HOME) + strlen(persist_file) + 1) * sizeof(char)); /* null terminated */
  strcpy(config_file, getenv("HOME"));
  strcat(config_file, XDG_CONFIG_PATH_FROM_HOME);
  strcat(config_file, persist_file);
  config_file[strlen(getenv("HOME")) + strlen(XDG_CONFIG_PATH_FROM_HOME) + strlen(persist_file)]='\0';
  printf("config file is %s\n",config_file);

  /* category dir: read from file, or if absent malloc and strcpy from
     category root dir */
  FILE *persist;
  if ((persist = fopen(config_file, "a+b"))) /* create if missing */
  {
    fseek(persist, 0, SEEK_SET); /* some libc's start at end of file. so seek back. */
    int len=line_length(persist);
    /* seek back again after reading */
    fseek(persist, 0, SEEK_SET);
    if(len==0) {
      /* file was empty or didn't exist, so start out in category_root_dir this time around */
      category_dir=(char *)malloc( (strlen(category_root_dir) + 1) * sizeof(char));
      strcpy(category_dir,category_root_dir);
    }
    else {
      /* allocate length and read string into memory */
      category_dir=(char *)malloc( (len + 1) * sizeof(char));
      /* read that line into memory, sans newline character */
      fgets(category_dir, len + 1, persist);
      printf("%s Opened successfully\n",config_file);
      
    }
    fclose(persist);
    printf("Category dir is %s\n",category_dir);
  }
  else
  {
    printf("Category dir is %s\n",category_dir);
    fprintf(stderr, "Error: unable to read or create persistance file.\nMake sure ~/.config is a directory that exists.\n");
  }
}


#ifdef USE_NATIVE_FILE_CHOOSER
void fileChosen(char *path) {
  if(strlen(path) > 0) {
    fprintf(stderr,"fixme: I am almost certain that this place in code is causing a memory leak, but I need to figure out why it causes double free errors then\n");
//THIS IS ALMOST DEFINITELY A MEMORY LEAK BUT I AM DEBUGGING
//      free(category_dir);
    category_dir=(char*)malloc((strlen(path) + 1) * sizeof(char));
    strcpy(category_dir,path);
    printf("Category dir is %s\n", category_dir);
    categoryname->label(category_dir);
    rightcol->redraw();
  }
}
#else
void fileChosen(Fl_File_Chooser* fileChooser, void*) {
  printf("is shown: %d\n", fileChooser->shown());
  if (!fileChooser->shown()) {
    if(strlen(fileChooser->value(1)) > 0) {
      fprintf(stderr,"fixme: I am almost certain that this place in code is causing a memory leak, but I need to figure out why it causes double free errors then\n");
//THIS IS ALMOST DEFINITELY A MEMORY LEAK BUT I AM DEBUGGING
//      free(category_dir);
      category_dir=(char*)malloc((strlen(fileChooser->value(1)) + 1) * sizeof(char));
      strcpy(category_dir,fileChooser->value(1));
      printf("Category dir is %s\n",category_dir);
      categoryname->label(category_dir);
      rightcol->redraw();
    }
    delete fileChooser;
  }
}

#endif


/* callback to quit */
void quit_app_callback(Fl_Widget*, void*) {
  cleanup();
  exit(0);
}

#ifdef USE_NATIVE_FILE_CHOOSER
/* "native" picker */
void showFileChooser(Fl_Widget*, void*) {

  Fl_Native_File_Chooser fileChooser;
  fileChooser.title("Select a Directory");
  fileChooser.type(Fl_Native_File_Chooser::BROWSE_DIRECTORY);
  fileChooser.directory(category_root_dir); 
  // Show file chooser
  switch (fileChooser.show()) {
  case -1: fprintf(stderr, "Error: %s\n", fileChooser.errmsg()); break;
  case 1: break; // Cancel
  default:
    if (fileChooser.filename()) {
      fileChosen((char*)fileChooser.filename());
                  }
    break;
  }
}


  
/*  Fl_Native_File_Chooser* fileChooser = new Fl_Native_File_Chooser(category_root_dir, "*.*", Fl_File_Chooser::DIRECTORY, "Choose directory");
  fileChooser->sort=fl_casealphasort;
  fileChooser->preview(0);
  fileChooser->callback(fileChosen);
  fileChooser->show();
  }*/
#else
/* need to make a native one instead to get unicode */
void showFileChooser(Fl_Widget*, void*) {
  Fl_File_Chooser* fileChooser = new Fl_File_Chooser(category_root_dir, "*.*", Fl_File_Chooser::DIRECTORY, "Choose directory");
  fileChooser->sort=fl_casealphasort;
/*  fileChooser->preview(0); */
  /* these next two calls seem to BOTH be necessary to make it not highlight
     the directory path in the text input field. */
  fileChooser->value("");
  fileChooser->directory(category_root_dir);
  fileChooser->show();
}
#endif

#if defined(CUSTOM_TYPEFACE)
void setup_main_font() {
  /* this is not especially necessary on FLTK 1.4 (if built with
     --enable-pango), but is vital on 1.3.x for unicode text to render
     since the given font needs to have every required glyph in it. */
  /* FL_NORMAL_SIZE=ORIG_FL_NORMAL_SIZE; */
  Fl::set_font(FL_HELVETICA, CUSTOM_TYPEFACE );
  Fl::set_font(FL_HELVETICA_BOLD, CUSTOM_TYPEFACE );
  Fl::set_font(FL_HELVETICA_ITALIC, CUSTOM_TYPEFACE );
  Fl::set_font(FL_HELVETICA_BOLD_ITALIC, CUSTOM_TYPEFACE );
  return;
}
#endif

int main(int argc, char* argv[]) {
  if(argc > 1) {
    target=argv[1];
  }
  else
  {
    target=NULL;
  }

/* To center window on screen, need to get info on window & screen first. */
  int screen_x, screen_y, screen_w, screen_h;
/* Uncomment this line and comment the one below if you do care about
   working area: */
/*Fl::screen_work_area(&screen_x, &screen_y, &screen_w, &screen_h);*/
  Fl::screen_xywh(screen_x, screen_y, screen_w, screen_h);
  
#if defined(CUSTOM_TYPEFACE)
  setup_main_font();
#endif
  /* Fl::get_system_colors(); */
  /* we are going to force the 'none' style to look more like my Motif/CDE
     desktop */
  Fl::background(174,178,195);
  Fl::background2(255,247,233);
  /* calculate category dirs and allocate/assign them
    (global vars *category_dir and *category_root_dir) */
  initialize_category_dir();
  
  /* construct the main window */
  Fl_Window *window = new Fl_Window(420,92);
  window->xclass("categorize_mcomix");

  font_init();
  /* handle custom font creation (see preprocessor defines up top) */
  Fl_Group *grp = new Fl_Group(0,0,420,92);
  Fl_Group *leftcol = new Fl_Group(0,0,50,92);
  Fl_Box *filelabel = new Fl_Box(0, 0, 0, 13 );
  Fl_Box *categorylabel = new Fl_Box(0, 13, 0, 13 );
  widget_setup((Fl_Widget *)filelabel);
  widget_setup((Fl_Widget *)categorylabel);
  filelabel->label("File:");
  categorylabel->label("Category:");
  filelabel->align(FL_ALIGN_RIGHT);
  categorylabel->align(FL_ALIGN_RIGHT);
  Fl_Button *cancelbutton = new Fl_Button(0, 26, 50, 66, "Cancel");
  box_button_setup(cancelbutton);
  leftcol->end();
  rightcol = new Fl_Group(60, 0, 360, 92); // y=52 for start of choose category button i think
  Fl_Box *filename = new Fl_Box(60, 0, 0, 13 );
  /* categoryname is global for convenience */
  categoryname = new Fl_Box(60, 13, 0, 13 );
  widget_setup((Fl_Widget *)filename);
  widget_setup((Fl_Widget *)categoryname);
  if(argc > 0) {
    filename->label(argv[1]);
  }
  else {
    filename->label("NONE!!!!!!");
  }
  categoryname->label(category_dir);
  filename->align(FL_ALIGN_RIGHT);
  categoryname->align(FL_ALIGN_RIGHT);
  Fl_Button *choosebutton = new Fl_Button(60, 26, 360, 21, "Choose Category");
  /* press enter and this widget intercepts it, no matter what keyboard focus is */
  Fl_Button *okbutton = new Fl_Return_Button(60, 47, 360, 47, "OK");
    /* Fl_Button *okbutton = new Fl_Button(60, 47, 360, 47, "OK"); */
  box_button_setup(choosebutton);
  box_button_setup(okbutton);
  rightcol->end();
  rightcol->resizable(okbutton);
  leftcol->resizable(cancelbutton);
  grp->end();
  window->resizable(grp);
  window->label("Categorize");
  cancelbutton->callback(quit_app_callback);
  choosebutton->callback(showFileChooser);
  okbutton->callback(do_linking);
  choosebutton->take_focus(); /* when widget starts, space will open chooser, enter will do the "ok" function */
  window->end();
  
  int dec_width  = window->decorated_w();
  int dec_height = window->decorated_h();

/*  int win_init_x = ((screen_w - dec_width) / 2);
    int win_init_y = ((screen_h - dec_height) / 2);*/
  int win_init_x=(screen_w - dec_width-5);
  int win_init_y=(screen_h-dec_height-16);
  window->position(win_init_x,win_init_y);

/*  window->show(argc, argv);*/
  window->show();
/* show() without those arguments lets us use our own */
  return Fl::run();
}

void widget_setup(Fl_Widget *widget)
{
  widget->labelfont(fl_font());
  /* widget->labelfont(FL_BOLD+FL_ITALIC); */
#if defined(CUSTOM_TYPEFACE2_SIZE)
  widget->labelsize(CUSTOM_TYPEFACE2_SIZE);
#else
  widget->labelsize(11);
#endif
}

void font_init()
{
  /* YUCK. todo: Fix this ugly preprocessor garbage for font selection
   at compile-time. */
#if defined(CUSTOM_TYPEFACE2)
  Fl::set_font(FL_FREE_FONT, CUSTOM_TYPEFACE2);
#if defined(CUSTOM_TYPEFACE_SIZE)
  fl_font(FL_FREE_FONT, CUSTOM_TYPEFACE_SIZE);
#else
  fl_font(FL_FREE_FONT, 11); /* default to 11px */
#endif /* defined(CUSTOM_TYPEFACE2_SIZE) */
#else /* default to FL_HELVETICA if no custom font is defined */
#if defined(CUSTOM_TYPEFACE_SIZE)
  fl_font(FL_HELVETICA, CUSTOM_TYPEFACE_SIZE);
#else
  fl_font(FL_HELVETICA, 11); /* default to 11px */
#endif /* defined(CUSTOM_TYPEFACE2_SIZE) */
#endif /* defined(CUSTOM_TYPEFACE) */
}

void box_button_setup(Fl_Button *button)
{
  /* button->button(FL_UP_BOX); */
  widget_setup((Fl_Widget *) button);
}


/* callback to do everything */
void do_linking(Fl_Widget*, void*){
  if(target) {
    /* target isn't null: an argument was actually passed to the program */
    persist_file_write(config_file, category_dir);
    do_link(target, category_dir);
    /* now quit */
    cleanup();
    exit(0);
  }
  else {
    fprintf(stderr,"E: No file was chosen.\n");
    alert((char *)"Warning: No input file was given as an argument!\n\nNothing done..");
  }
  return;
}

int line_length(FILE *file)
{
  char a;
  int i=0;
  a=getc(file); /* length 0 means empty file */
  while(a != '\n' && a != EOF)
  {
    a=getc(file);
    i++;
  }
  return i;
}


char *persist_file_read(char *path)
{
  FILE *persist_file=fopen(path, "rb"); /* 'b' is for windows */
  char *string;
  if(persist_file==NULL)
  {
    return NULL;
  }
  else
  {
    /* get first line length sans newline */
    int pathlen=line_length(persist_file);
    /* seek to beginning of file again */
    fseek(persist_file, 0, SEEK_SET);
    string=(char *)malloc((pathlen + 1) * sizeof(char));
    /* read that line into memory, sans newline character */
    fgets(string, pathlen + 1, persist_file);
    printf("%s Opened successfully\n",path);
    fclose(persist_file);
  }
  return string;
}

int persist_file_write(char *path, char *string)
{
  FILE *persist_file=fopen(path, "wb");
  if(persist_file==NULL)
  {
    /* error */
    fprintf(stderr,"Error opening persistence file to write on exit.\n");
    return -1;
  }
  fprintf(persist_file,"%s\n",string);
  fclose(persist_file);
  return 0;
}

void cleanup() {
  free(category_dir);
  free(category_root_dir);
  free(config_file);
  return;
}

void do_link(char *origin, char *destpath){
  /* first get basename of origin */
  char *origin_base_name;
  char *working_buf;
  char *to_free;
  char *combined_destination;
  /* laziness here: we just allocate an equal amount of space as the source string since that should always be enough. */
  working_buf=(char*)malloc((strlen(origin) + 1) * sizeof(char));
  to_free=working_buf;
  /* basename() can modify original string; therefore we do a copy first: */
  strcpy(working_buf, origin);
  working_buf=basename(working_buf);
  origin_base_name=(char*)malloc((strlen(working_buf) + 1) * sizeof(char));
  strcpy(origin_base_name,working_buf);
  free(to_free);

  /* now we have one allocated block of memory: the basename'd string. */
  /* the to_free and working_buf pointers are not valid at this point. */
  /* next we need to concatenate it to destpath. */
  combined_destination=(char*)malloc( ( sizeof(origin_base_name) + sizeof(destpath) + 2) * sizeof(char)); /* +2 because '/' and terminator */
  strcpy(combined_destination, destpath);
  strcat(combined_destination, "/");
  strcat(combined_destination, origin_base_name);
  category_root_dir[strlen(getenv("HOME")) + strlen(subdir_root)]='\0';
  free(origin_base_name);

  /* now we have our combined string: [destpath]/[origin_base_name], and
     once more have one allocated block: combined_destination. 
     Next, we need to make the link. */

  printf("LINKING: %s -> %s\n",origin,combined_destination);

  
  /* *ON LINUX*, and *POSSIBLY ONLY ON LINUX,* we can use stat() to check
     the st_dev field and see if two files are on the same device (and
     determine whether to hard or soft link). See stat(2). It looks like
     at least OpenBSD also provides st_dev, though. */
  struct stat *origin_stats=(struct stat *)malloc(sizeof(struct stat));
  struct stat *dest_stats=(struct stat *)malloc(sizeof(struct stat));
  struct stat *combined_destination_stats=(struct stat *)malloc(sizeof(struct stat));
  int check;
  lstat(combined_destination, combined_destination_stats); /* the destination */
  if(combined_destination_stats != NULL) {
    /* check if symbolic link, remove it if it is. */
    if ((combined_destination_stats->st_mode & S_IFMT)==S_IFLNK) {
      /* I was hitting some weird sort of race condition, so this slows the program down a wee bit to avoid a glitch with unlink(). */
      struct timespec TS;
      /* reminder: 0.1 seconds is 100,000,000 nS */
      TS.tv_sec=(time_t)0;
      TS.tv_nsec=(long)10000;
      nanosleep(&TS, NULL);
      check=unlink(combined_destination);
      if(check != 0) {
        fprintf(stderr,"E: Something happened wrong when trying to remove symbolic link at %s. errno %d\n",combined_destination,errno);
      }
      else {
        printf("Removed existing link to make room for new file.\n");
      }
    }
  }
  /* if it was a real file, we'll catch that in a moment. */
  free(combined_destination_stats);
  
  if(origin_stats == NULL || dest_stats == NULL) {
    fprintf(stderr,"E: malloc() failed!\n");
    return;
  }
/* lstat doesn't follow symlinks. */
  lstat(origin, origin_stats); /* the original file */
  lstat(destpath, dest_stats); /* the directory to contain our new link */
  if(origin_stats == NULL || dest_stats == NULL) {
    fprintf(stderr, "E: something went wrong performing lstat().\n");
  }
  
  printf("DEBUG: st_dev: %ld vs. %ld\n",origin_stats->st_dev,dest_stats->st_dev);
  if(origin_stats->st_dev != dest_stats->st_dev) {
    /* we need to do a symbolic link to bridge partitions. */
    /* first we need to check if a symlink at the destination already exists,
       since the function doesn't let us force overwrite links. */
    printf("Source and destination are on different partitions. Symbolic linking.\n");
    check=symlink(origin, combined_destination);
    if(check != 0) {
      fprintf(stderr, "ERROR: symbolic link creation failed...\n");
    }
  }
  else {
    printf("Source and destination are on the same partition. Hard linking.\n");
    /* note: todo: we currently are hardlinking to symbolic links. Might want to
       follow these in the future and then check if they're on the same device. */
    /*linkat is stupid and requires opening fd's before it will work even though
      i don't want to follow relative links. But posix link(3posix) manpage says this
      should work.  */
    check=linkat(AT_FDCWD, origin, AT_FDCWD, combined_destination, AT_SYMLINK_FOLLOW);
    if(check!=0) {
      fprintf(stderr, "ERROR: hard link creation failed...\n");
    }
  }
  printf("Link attempted. Check if it worked.\n");  
  free(origin_stats);
  free(dest_stats);
  free(combined_destination);
  /* return to do_linking */
}

void alert(char *text) {
#if defined(CUSTOM_TYPEFACE2)
    Fl::set_font(FL_HELVETICA, CUSTOM_TYPEFACE2 );
#if defined(CUSTOM_TYPEFACE2_SIZE)
    FL_NORMAL_SIZE=CUSTOM_TYPEFACE2_SIZE;
#endif
#endif
    fl_alert(text);
    /* now revert font choices */
#if defined(CUSTOM_TYPEFACE)
#if defined(CUSTOM_TYPEFACE_SIZE)
    /* set back to size used inside of the file chooser */
    FL_NORMAL_SIZE=CUSTOM_TYPEFACE_SIZE;
#endif
    setup_main_font();
#endif
}
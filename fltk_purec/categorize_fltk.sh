#! /bin/sh
# wrapper for the C stuff
# I think this should be POSIX-compliant.
CATEGORY_ROOT="$HOME"'/root/'

TARGET=

CATEGORIZE_CATEGORY_ROOT_DIR="$HOME"'/root/'
CATEGORIZE_CONFIG_FILE="$HOME"'/.config/categorize'
CATEGORIZE_CATEGORY_DIR="$(head -n 1 "$CATEGORIZE_CONFIG_FILE")"

ORIGIN="$1"
BASEFILE="$(basename "$ORIGIN")"

COMBINED="$(categorize_fltk_bin "$ORIGIN")"'/'"$BASEFILE"

# handle symlink vs hardlink
link_files() {
  # check if same partition or not
  # check file path for input and dir path for output
  if [ "$(df "$1" --output=target | tail -n 1)" = "$(df "$(dirname "$2")" --output=target | tail -n 1)" ]; then
    # same filesystem, hardlink
    echo 'hardlinking.'
    # -L: dereference if target is itself a symlink
    ln -L "$1" "$2"
  else
    # different filesystem, symlink
    echo 'symlinking.'
    ln -s "$1" "$2"
  fi
}


#if [ "$?" -eq 0 ]; then
  if [ -L "$COMBINED" ]; then
    # if file exists, and is a symbolic link; remove and replace with our hardlink
    rm "$COMBINED"
    #    ln "$(readlink -f "$ORIGIN")" "$COMBINED"
    link_files "$ORIGIN" "$COMBINED"
  elif [ -e "$COMBINED" ]; then
    # file exists already and isn't a symlink... do nothing for now, I guess? Change this here.
    echo "Doing nothing."
    true
  else
  # make hardlink
    #  ln "$(readlink -f "$ORIGIN")" "$COMBINED"
    link_files "$ORIGIN" "$COMBINED"
  #fi
#else
#  1>&2 echo "E: categorize gui crashed or cancelled"
#  zenity --info --text "categorize gui crashed or cancelled"
fi
